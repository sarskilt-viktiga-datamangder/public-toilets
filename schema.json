{
  "@context": [
    "http://www.w3.org/ns/csvw",
    {
      "@language": "sv",
      "license": { "@id": "dc:license", "@type": "@id" },
      "modified": { "@id": "dc:modified", "@type": "xsd:date" }

    }
  ],
  "dc:title": "CSV schema för offentliga toaletter",
  "dc:subject": ["offentliga toaletter"],
    "license": "http://creativecommons.org/licenses/by/4.0/",
    "modified": "2024-03-05",

  "tableSchema": {
    "columns": [
      {
        "name": "source",
        "titles": "Källa",
        "dc:description": "Ange SCB:s kommunkod för er kommun eller organisationsnumret för verksamheten. Exempel: \"1401\" för Härryda kommun eller organisationsnummer \"21 20 00-12 64\".",
        "datatype": "integer",
        "required": true
      },
      {
        "name": "id",
        "titles": ["Objektidentitet"],
        "dc:description": "Ange en unik och stabil identifierare för den offentliga toaletten inom er kommun. Det viktiga är att identifieraren är unik (tänk unik i hela världen), och att den är stabil över tid - dvs att den inte ändras mellan uppdateringar. Om en offentlig toalett avvecklas ska identifieraren inte återanvändas på en annan offentlig toalett.",
        "datatype": "string",
        "required": true, 
        "propertyUrl" : "dct:identifier"
      },
      {
        "name": "name",
        "titles": "Offentliga toalettens namn",
        "dc:description": "Den offentliga toalettens namn, exempel för ett en offentlig toalett i Region Gotland \"Tingstäde träsk badplats\".",
        "datatype": "string",
        "required": true
      },
      {
        "name": "latitude",
        "titles": "Latitud",
        "dc:description": "Latitud anges per format som WGS84-specifikation.",
        "datatype": "decimal",
        "required": true,
        "propertyUrl": "schema:latitude"
      },
      {
        "name": "longitude",
        "titles": "Longitude",
        "dc:description": "Longitud anges per format som WGS84-specifikation.",
        "datatype": "decimal",
        "required": true, 
        "propertyUrl": "schema:longitude"
      },
      {
        "name": "email",
        "titles": "Email",
        "dc:description": "E-postadress för vidare kontakt, anges med gemener och med @ som avdelare. Använd ej mailto: eller andra HTML-koder. Exempel: info@toreboda.se",
        "datatype": "string",
        "propertyUrl": "schema:email",
        "required": true
      },
      {
        "name": "visit_url",
        "titles": "Länk till mer information",
        "dc:description": "Länk till allmän besöksinformation för platsen, t.ex. verksamhetens öppettider eller liknande.",
        "datatype": "url",
        "propertyUrl":"schema:url"
      },
      {
        "name": "wikidata",
        "titles": "Wikidata",
        "dc:description": "Referens till Wikidata-artikel/entry om platsen. Se kapitel \"3. Wikidata\" i detta dokument. Använd den sk. Q-koden till entiteten på Wikidata. Exempel för att länka till offentlig toalett i Raahe, Finland på Wikidata: Q55596141 Q-kod anges utan mellanslag, bindestreck eller skiljetecken och består alltid av bokstaven Q följt av ett antal siffror.",
        "datatype": "string"
      },
      {
        "name": "updated",
        "titles": "Senast uppdaterad",
        "dc:description": "Ett datum, enligt ISO 8601, som anger när informationen om den offentlig toaletten senast uppdaterades",
        "datatype": "date"
      },
      {
        "name": "description",
        "titles": "Beskrivning",
        "dc:description": "Kort beskrivning av den specifika offentliga toalettens placering eller särskilda förutsättningar. Exempel: \"Ligger på bibliotekets baksida\" eller \"Felanmäl direkt till verksamhetens personal\" Undvik att beskriva sådant som redan framgår i specifikationen.",
        "datatype": "string",
        "propertyUrl" : "dct:description"
      },
      {
        "name": "street",
        "titles": "Gatuadress",
        "dc:description": "Gatuadress till platsen, exempelvis \"Strandvägen\". Ange inte nummer på gata eller fastighet, det anges i attributet \"housenumber\". Mappar mot Open Street Maps \"addr:street\" läs mer på Key:addr på Open Streetmaps webbplats.",
        "datatype": "string",
        "propertyUrl": "schema:streetAddress"
      },
      {
        "name": "housenumber",
        "titles": "Gatunummer",
        "dc:description": "Gatunummer eller husnummer med bokstav. Exempelvis \"1\" eller \"42 E\". Mappar mot Open Street Maps \"addr:housenumber\" läs mer på: Key:addr på Open Streetmaps webbplats.",
        "datatype": "string"
      },
      {
        "name": "postcode",
        "titles": "Postnummer",
        "dc:description": "Postnummer anges med fem siffor utan mellanslag, exempelvis \"46430\". Detta mappar mot Open Street Maps \"addr:postcode\", läs mer på: Key:addr på Open Streetmaps webbplats.",
        "datatype": {
          "base": "integer",
          "minimum": 10000,
          "maximum": 99999
          },
        "propertyUrl": "schema:postalCode"  
      },
      {
        "name": "city",
        "titles": "Postort",
        "dc:description": "Postort, exempelvis \"Mellerud\". Mappar mot Open Street Maps \"addr:city\", läs mer på: Key:addr på Open Streetmaps webbplats.",
        "datatype": "string", 
        "propertyUrl": "schema:addressLocality"
      },
      {      
        "name": "country",
        "titles": "Land",
        "dc:description": "Land där den offentliga toaletten finns. Skall anges enligt ISO3166-1. För Sverige ange \"SE\". Fältet mappar mot Open Streetmaps \"addr:country\".",
        "datatype": "string"
      },
      {
        "name": "wc",
        "titles": "WC",
        "dc:description": "Antal vattenklosetter.",
        "datatype": {
          "base": "integer",
          "minimum": 0,
          "maximum": 99
          }
      },
      {
        "name": "urinal",
        "titles": "Urinoar",
        "dc:description": "Antal urinoarer.",
        "datatype": {
          "base": "integer",
          "minimum": 0,
          "maximum": 99
          }
      },
      {
        "name": "outhouse",
        "titles": "Utedass",
        "dc:description": "Antal torrklosetter (dass).",
        "datatype": {
          "base": "integer",
          "minimum": 0,
          "maximum": 99
          }
      },
      {
        "name": "rwc",
        "titles": "RWC",
        "dc:description": "Antal toaletter som uppfyller Plan- och bygglagens krav på tillgängliga hygienrum.",
        "datatype": {
          "base": "integer",
          "minimum": 0,
          "maximum": 99
          }
      },
      {
        "name": "GenderType",
        "titles": "Könstillgänglighet",
        "dc:description": "Anger om den offentliga toaletten är för unisex, kvinnor eller män.",
        "datatype": {
          "base": "string", 
          "format": "UNISEX|FEMALE|MALE"
        }
      },
      {
        "name": "lighting",
        "titles": "Belysning",
        "dc:description": "Anger om den offentliga toaletten har belysning.",
        "datatype": "boolean"
      },
      {
        "name": "water",
        "titles": "Vatten",
        "dc:description": "Anger om det finns tillgång till vatten för att exempelvis tvätta händerna.",
        "datatype": "boolean"
      },
      {
        "name": "drinking_water",
        "titles": "Dricksvatten",
        "dc:description": "Anger om det finns dricksvatten på platsen i någon form.",
        "datatype": "boolean"
      },
      {
        "name": "trashbin",
        "titles": "Soptunna",
        "dc:description": "Anger om det finns en soptunna i någon form på platsen.",
        "datatype": "boolean"
      },
      {
        "name": "change_table_child",
        "titles": "Skötbord för barn",
        "dc:description": "Anger om det finns ett skötbord anpassat för barn.",
        "datatype": "boolean"
      },
      {
        "name": "wc_child",
        "titles": "Toalett för barn",
        "dc:description": "Anger om toaletten är anpassad för barn, eller att det finns en mindre toalett för barn.",
        "datatype": "boolean"
      },
      {
        "name": "change_table_adult",
        "titles": "Skötbord för vuxna",
        "dc:description": "Anger om det finns ett skötbord anpassat för vuxna.",
        "datatype": "boolean"
      },
      {
        "name": "rwc-alarm",
        "titles": "RWC-larm",
        "dc:description": "Anger om det finns RWC-larm installerat.",
        "datatype": "boolean"
      },
      {
        "name": "door_opener",
        "titles": "Dörröppnare",
        "dc:description": "Anger om det finns RWC-anpassad automatisk dörröppnare installerad.",
        "datatype": "boolean"
      },
      {
        "name": "accessibility",
        "titles": "Tillgänglighet",
        "dc:description": "Ytterligare beskrivning om hur tillgänglighetsanpassad den offentliga toaletten är.",
        "datatype": "text"
      },
      {
        "name": "validFrom",
        "titles": "Öppningsdatum",
        "dc:description": "Anger när på året, i dag och månad (DD MMMM), den offentliga toaletten öppnas. Exempel: \"1 maj\". Året-runt-öppna lämnas tomma.",
        "datatype": "dateTime"
      },
      {
        "name": "validThrough",
        "titles": "Stängningsdatum",
        "dc:description": "Anger när på året, i dag och månad (DD MMMM), den offentliga toaletten stängs. Exempel: \"30 september\". Året-runt-öppna lämnas tomma.",
        "datatype": "dateTime"
      },
      {
        "name": "openingHours",
        "titles": "Öppettider i fritext",
        "dc:description": "Beskriver när under dygnet den offentliga toaletten är öppen. Exempel: \"Tillgång endast under bibliotekets öppettider\". Dygnet-runt-öppna lämnas tomma.",
        "datatype": "text"
      },
      {
        "name": "opens",
        "titles": "Öppningstid",
        "dc:description": "Anger när på dygnet, i timme och minut (hh:mm), den offentliga toaletten öppnas. Exempel: \"05:00\". Dygnet-runt-öppna lämnas tomma.",
        "datatype": "dateTime"
      },
      {
        "name": "closes",
        "titles": "Stängningstid",
        "dc:description": "Anger när på dygnet, i timme och minut (hh:mm), den offentliga toaletten stängs. Exempel: \"22:00\". Dygnet-runt-öppna lämnas tomma.",
        "datatype": "dateTime"
      },
      {
        "name": "service_interval",
        "titles": "Underhållsfrekvens",
        "dc:description": "Ange i vilken frekvens den offentliga toaletten städas.",
        "datatype": {
          "base": "string", 
          "format": "UNKNOWN|IRREG|YEARLY|QUARTERLY|MONTHLY|WEEKLY|DAILY"
        }
      },
      {
        "name": "fee",
        "titles": "Avgift",
        "dc:description": "Anger om den offentliga toaletten är avgiftsbelagd.",
        "datatype": "boolean"
      },
      {
        "name": "fee_amount",
        "titles": "Avgiftens belopp",
        "dc:description": "Beskriver avgiftens belopp i decimaltal mellan \"0.01\" till \"999\". Avgiftsfria lämnas tomma.",
        "datatype": {
          "base": "float",
          "minimum": 0.01,
          "maximum": 999
          }
      },
      {
        "name": "fee_currency",
        "titles": "Avgiftens valuta",
        "dc:description": "Beskriver avgiftens valuta. Skall anges enligt ISO 4217. För Sverige ange \"SEK\". Avgiftsfria lämnas tomma.",
        "datatype": "text"
      },
      {
        "name": "fee_payment_method",
        "titles": "Avgiftens betalningsmetoder",
        "dc:description": "Beskriver möjliga betalningsmetoder för avgiften",
        "datatype": {
          "base": "string",
          "format": "Cash|DirectDebit|Swish"
        }
      },
      {
        "name": "fee_description",
        "titles": "Övrig information om avgiften",
        "dc:description": "Beskriver övrig information om avgiften. Exempel \"Betalas till personal i kassan.\"",
        "datatype": "text"
      },
      {
        "name": "td_url",
        "titles": "Tillgänglighetsdatabasen",
        "notes": "Länk till Tillgänglighetsdatabasen, TD (https://www.t-d.se/)",
        "datatype": "url"
      },
      {
        "name": "image_url",
        "titles": "Bild",
        "notes": "Länk till en bild i formaten .jpg, .png eller .webp. Licens ska vara allmän egendom enligt CC0 (https://creativecommons.org/public-domain/cc0/).",
        "datatype": "url"
      },
      {
        "name": "report_phone",
        "titles": "Telefon för felanmälan",
        "notes": "Telefonnummer för frågor, synpunkter och/eller felanmälan.",
        "datatype": "phone"
      },
      {
        "name": "report_url",
        "titles": "Länk för felanmälan",
        "notes": "Länk till tjänst för frågor, synpunkter och/eller felanmälan.",
        "datatype": "url"
      },
      {
        "name": "containedInPlace",
        "titles": "Placerad i en annan plats",
        "notes": "Anger om den offentliga toaletten är placerad i en annan plats med egna öppettider.",
        "datatype": "boolean"
      },
      {
        "@context": "https://schema.org",
        "@type": "CivicStructure",
        "name": "Offentlig byggnad",
        "openingHours": [
          "Mo-Fr 09:00-17:30",
          "Sa 09:00-12:00"
        ]
      },
      {
        "name": "temp_closed",
        "titles": "Tillfälligt stängd",
        "dc:description": "Anger om den offentliga toaletten är tillfälligt ur funktion eller stängd, till exempel för reparation, underhåll eller ombyggnation. True innebär att den är stängd, False eller tomt värde att den är öppen",
        "datatype": "boolean"
      }
    ],
    "primaryKey": ["source", "id"],
    "aboutUrl": "/public-toilets/{source}/{id}"
  }
}
