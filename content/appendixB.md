# Exempel i JSON

Samma entitet som i Appendix A, fast nu som JSON.

```json
{
    "id":  "",
    "source": "",
    "type": "",
    "latitude":  "58.399237",
    "longitude":  "15.417352",
    "description":  "",
    "street":  "",
    "postalcode":  "99999",
    "city":  "City",
    "email":  "",
    "URL":  "https://",
    "resource":  "",      
}
```
