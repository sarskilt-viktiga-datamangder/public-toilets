**Bakgrund**

Syftet med denna specifikation är att beskriva information om offentliga toaletter på ett enhetligt och standardiserat vis.

Det finns idag många offentliga toaletter i det offentliga rummet, fristående och i olika inrättningar men beskrivningar, information och data om dessa finns inte lättillgängligt att hitta och använda.

Specifikationen syftar till att ge aktörer som kommuner, regioner och myndigheter men även privata aktörer och civila aktörer i Sverige en möjlighet att enkelt kunna sätta samman och publicera datamängd(er) som beskriver offentliga toaletter - oavsett ägare eller leverantör. Den syftar även till att göra det enklare för internationella användare som är tagare av datamängden.

Följande har deltagit:

**[Dataverkstad](https://www.vgregion.se/ov/dataverkstad/)** - Modellering och rådgivning.<br>



