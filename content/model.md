# Datamodell


<div class="warning" title="Under utveckling">
Denna sektion är under utveckling och ännu inte i fas med schema.json, se den filen för en mer aktuell beskrivning.
</div>


Datamodellen är tabulär, det innebär att varje rad motsvarar exakt en entitet och varje kolumn motsvarar en egenskap för den beskrivna entiteten. XX attribut är definierade, där de första Y är obligatoriska. Speciellt viktigt är att man anger de obligatoriska fält i datamodellen som anges. 


<div class="note" title="1">
Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige.


</div>

<div class="ms_datatable">

| Namn  | Kardinalitet      | Datatyp                         | Beskrivning|
| -- | :---------------:| :-------------------------------: | ---------------------- |
|[**source**](#source)|1|[heltal](#heltal)|**Obligatoriskt** - Ange SCB:s kommunkod för er kommun eller organisationsnumret för verksamheten. Exempel: "1401" för Härryda kommun eller organisationsnummer “212000-1264”.|
|[**id**](#id)|1|text| **Obligatoriskt** - Ange en unik och stabil identifierare för offentliga toaletten inom er kommun. Det viktiga är att identifieraren är unik (tänk unik i hela världen), och att den är stabil över tid - dvs att den inte ändras mellan uppdateringar. Om en offentlig toalett avvecklas ska identifieraren inte återanvändas på en annan offentlig toalett.|
|[**name**](#name)|1|text|**Obligatoriskt** - Den offentliga toalettens namn, exempel för ett en offentlig toalett i Region Gotland "Tingstäde träsk badplats”.|
|[**latitude**](#latitude)|1|decimaltal| **Obligatoriskt** - Latitud anges per format som WGS84-specifikation.|
|[**longitude**](#longitude)|1|decimaltal| **Obligatoriskt** - Longitud anges per format som WGS84-specifikation.|
|[**email**](#email)|1|text| **Obligatoriskt** - E-postadress för vidare kontakt, anges med gemener och med @ som avdelare. Använd ej mailto: eller andra HTML-koder. Exempel: [info@toreboda.se](mailto:info@toreboda.se)|
|[visit_url](#visit_url)|0..1|URL| Länk till allmän besöksinformation för platsen, t.ex. verksamhetens öppettider eller liknande.|
|[wikidata](#wikidata)|0..1|text| Referens till Wikidata-artikel/entry om platsen. Se kapitel “3. Wikidata" i detta dokument. Använd den sk. Q-koden till entiteten på Wikidata. Exempel för att länka till offentlig toalett i Raahe, Finland på Wikidata: Q55596141 Q-kod anges utan mellanslag, bindestreck eller skiljetecken och består alltid av bokstaven Q följt av ett antal siffror. |
|[updated](#update)|[dateTime](#datetime)|Ett datum som anger när informationen om den offentlig toaletten senast uppdaterades enligt [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html). Anges per år som YYYY, per månad som YYYY-MM eller per dag som YYYY-MM-DD.|
|[description](#description)|0..1|text| Kort beskrivning av den specifika offentliga toalettens placering eller särskilda förutsättningar. Exempel: "Ligger på bibliotekets baksida" eller "Felanmäl direkt till verksamhetens personal" Undvik att beskriva sådant som redan framgår i specifikationen.|
|[street](#street)|0..1|text| Gatuadress till platsen, exempelvis “Strandvägen”. Ange inte nummer på gata eller fastighet, det anges i attributet “housenumber”. Mappar mot Open Street Maps “addr:street” läs mer på [Key:addr på Open Streetmaps webbplats.](https://wiki.openstreetmap.org/wiki/Key:addr)|
|[housenumber](#housenumber)|0..1|text| Gatunummer eller husnummer med bokstav. Exempelvis “1” eller “42 E”. Mappar mot Open Street Maps “addr:housenumber” läs mer på: [Key:addr på Open Streetmaps webbplats.](https://wiki.openstreetmap.org/wiki/Key:addr)|
|postcode|0..1|text| Postnummer, exempelvis “46430”, mappar mot Open Street Maps “addr:postcode”, läs mer på: [Key:addr på Open Streetmaps webbplats.](https://wiki.openstreetmap.org/wiki/Key:addr)|
|city|0..1|text| Postort, exempelvis “Mellerud”. Mappar mot Open Street Maps “addr:city”, läs mer på: [Key:addr på Open Streetmaps webbplats.](https://wiki.openstreetmap.org/wiki/Key:addr)|
|country|0..1| text| Land där den offentliga toaletten finns. Skall anges enligt ISO3166-1. För Sverige ange “SE”. Fältet mappar mot Open Streetmaps “addr:country”.|
|[wc](#wc)|0..1|int|Antal vattenklosetter.|
|[urinal](#urinal)|0..1|int| Antal urinoarer.|
|[outhouse](#outhouse)|0..1|int| Antal torrklosetter (dass).|
|[rwc](#rwc)|0..1|integer| Antal toaletter som uppfyller Plan- och bygglagens krav på tillgängliga hygienrum. [https://www.boverket.se/sv/PBL-kunskapsbanken/regler-om-byggande/boverkets-byggregler/tillganglighet/hygienrum/](https://www.boverket.se/sv/PBL-kunskapsbanken/regler-om-byggande/boverkets-byggregler/tillganglighet/hygienrum/)|
|lighting|0..1|boolean| Beskriver om den offentliga toaletten har belysning.|
|[water](#water)|0..1|boolean| Anger om det finns tillgång till vatten för att exempelvis tvätta händerna.|
|[drinking_water](#drinking_water)|0..1|boolean| Anger om det finns dricksvatten på platsen i någon form.|
|[trashbin](#trashbin)|0..1|boolean| Anger om det finns en soptunna i någon form på platsen.|
|[unisex](#unisex)|0..1|boolean| Anger om den offentliga toaletten är unisex.|
|[change_table_child](#change_table_child)|0..1|boolean| Anger om det finns ett skötbord anpassat för barn.|
|[wc_child](#wc_child)|0..1|boolean| Anger om toaletten är anpassad för barn, eller att det finns en mindre toalett för barn.|
|[change_table_adult](#change_table_adult)|0..1|boolean| Anger om det finns ett skötbord anpassat för vuxna.|
|[rwc-alarm](#rwc-alarm)|0..1|boolean| Anger om det finns RWC-larm installerat.|
|[door_opener](#door_opener)|0..1|boolean| Anger om det finns RWC-anpassad automatisk dörröppnare installerad.|
|[accessability](#accessability)|0..1|text| Ytterligare beskrivning om hur tillgänglighetsanpassad den offentliga toaletten är.|
|[open_season](#open_season)|0..1|Text **?Datum-standard?**| Beskriver när under året den offentliga toaletten är öppen. Exempel: "1 maj - 30 september". Året-runt-öppna lämnas tomma.|
|[open_hours](#open_hours)|0..1|Text **?Tid-standard?**| Beskriver när under dygnet den offentliga toaletten är öppen. Exempel: "05:00-22:00" eller "Tillgång endast under bibliotekets öppettider". Dygnet-runt-öppna lämnas tomma.|
|[service_interval](#service_interval)|0..1|UNKNOWN/IRREG/ YEARLY/QUARTERLY/ MONTHLY/WEEKLY/DAILY | Ange i vilken frekvens den offentliga toaletten städas. **?När den senast städades?**|
|[fee](#fee)|0..1|text| Beskriver om den offentliga toaletten är avgiftsbelagd och hur man betalar. Exempel "5kr. Betala med mynt, betalkort och Swish" Avgiftsfria lämnas tomma.|
|[td_url](#td_url)|0..1|URL|[Länk till Tillgänglighetsdatabasen (TD).](https://www.t-d.se/)|
|[image_url](#image_url)|0..1|URL|Länk till en bild i formaten .jpg, .png eller .webp. **?Licens för bild - CC0?**|
|[report_phone](#report_phone)|0..1|phone|Telefonnummer för frågor, synpunkter och/eller felanmälan.|
|[report_url](#report_url)|0..1|URL|Länk till tjänst för frågor, synpunkter och/eller felanmälan.|
|[open](#open)|0..1|boolean|Anger om den offentliga toaletten är tillfälligt stängd, till exempel för underhåll eller ombyggnation.

</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, dvs inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.
 
### **decimal**
Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas då den tabulära modellen använder komma som separator.

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**

En länk till en webbsida där entiteten presenteras hos den lokala myndigheten eller organisationen.

Observera att man inte får utelämna schemat, d.v.s. "www.example.com" är inte en tillåten webbadress, däremot är "https://www.example.com" ok. Relativa webbadresser accepteras inte heller. (Ett fullständigt reguljärt uttryck utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

Kontrollera även innan du publicerar filen att det går att läsa in din fil utan problem. Det finns flera webbverktyg för att testa så kallad [parsing](https://sv.wikipedia.org/wiki/Parser) t.ex. [https://CSVLint.io](https://csvlint.io) som du kan använda för att testa din fil.

### **boolean**

Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad Boolesk” datatyp och kan antingen ha ett av två värden: TRUE eller FALSE, men aldrig båda.

### **dateTime** 

Reguljärt uttryck: **`\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d(?:\.\d+)?Z?`**

dateTime värden kan ses som objekt med heltalsvärdena år, månad, dag, timme och minut, en decimalvärderad sekundegenskap och en tidszonegenskap.

[XML schema](https://www.w3.org/TR/xmlschema-2/#dateTime) 
</br>

[IS0 8601](https://en.wikipedia.org/wiki/ISO_8601)

Värden som kan anges för att beskriva ett datum:

YYYY-MM-DD
</br>
YYYY-MM
</br>
YYYYMMDD 
</br>
</div>

Observera att ni inte kan ange: **YYYYMM**    

För att beskriva tider uttrycker ni det enligt nedan:

**T** representerar tid, **hh** avser en nollställd timme mellan 00 och 24, **mm** avser en nollställd minut mellan 00 och 59 och **ss** avser en nollställd sekund mellan 00 och 60.

Thh:mm:ss.sss eller	Thhmmss.sss 
</br>
Thh:mm:ss eller Thhmmss
</br>
Thh:mm eller Thhmm
</br>

</div>

Exempel - XX som äger rum kl 17:00 den 22 februari 2023 

</br>
2023-02-22T17:00

</br>
</br>

20230222T1700

</div>

## Förtydligande av attribut

### **id**

Reguljärt uttryck: **`/^[a-zA-Z_:1-9]*$/`**

Ange identfierare för entiteten. 

### source

Ange organisationsnummret utan mellanslag eller bindesstreck för organisationen. Exempel för Gullspångs kommun: **2120001637**.

### type

### latitude

WGS84 är den standard som det amerikanska systemet GPS använder för att beskriva en latitud på jordklotets yta. GPS används av många kartapplikationer. Latitud anges med ett heltal följt av en decimalpunkt “.” och 1 till 8 decimaler. Exempelvis “61.21657”. En angivelse av latitud som befinner sig på jordens södra hemisfär anges med negativt tal. Exempelvis “-53.78589”. Om koordinatens inledande heltal är noll, skall alltid nollan anges. 

### longitude

Longitud anges med ett heltal följt av en decimalpunkt följt av 1 till 8 decimaler. Exempelvis “88.40901”. En longitud som ligger väster om WGS84-systemets meridian, anges med negativt tal, Exempelvis: “-0.158101”. Om koordinatens inledande heltal är noll, skall nollan alltid anges.


### street

Entitetens adress.

### postalcode

Postnummer. Ange inte mellanslag. 

### city

Postort.

### email

Funktionsadress till den organisation som ansvarar för information om entiteten. Ange ej det inledande URI schemat mailto: eller HTML-koder i uttrycket. Exempel: example@domain.se

### resource

Länk till resurser. Ange inledande schemat https:// eller http://

### rwc

Antal toaletter som uppfyller Plan- och bygglagens krav på tillgängliga hygienrum. https://www.boverket.se/sv/PBL-kunskapsbanken/regler-om-byggande/boverkets-byggregler/tillganglighet/hygienrum/

 




