# Introduktion 

Med offentliga toaletter avses alla toaletter som finns tillgängliga i det offentliga rummet för alla att använda, med eller utan kostnad för brukaren.

Denna specifikation anger vilka fält som är obligatoriska och rekommenderade. T.ex. gäller att en publicist måste ange id, longitude och latitude. XX attribut kan anges varav Y är obligatoriska. 

I appendix A finns ett exempel på hur en entitet uttrycks i CSV. I appendix B uttrycks samma exempel i JSON. 

Denna specifikation definierar en enkel tabulär informationsmodell för entiteten. Specifikationen innefattar också en beskrivning av hur informationen uttrycks i formaten CSV och JSON. Som grund förutsätts CSV kunna levereras då det är ett praktiskt format och skapar förutsägbarhet för mottagare av informationen. 
